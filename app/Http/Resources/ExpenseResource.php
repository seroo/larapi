<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
           'description' => $this->description,
           'variety' => $this->variety,
           'place' => $this->place,
           'isJoint' => $this->isJoint,
           'amount' => $this->amount,
           'user_id' => $this->user_id,
           'created_at' => $this->created_at,
           'updated_at' => $this->updated_at
        ];
    }
}
